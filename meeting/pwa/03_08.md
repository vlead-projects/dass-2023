# Minutes Of Meeting
**Date: 22th February 2023
Time: 4:30 pm
Topic: Meeting on homepage PWA**

# Attendees
**Clients** --- Raj Agnihotri , Priya Raman
**TA** --- Karmanjyot Singh

**Team Members** ---
| Name | Email | Roll No. |
| ---- | ----- | -------- |
| Haran Raajesh | haran.raajesh@students.iiit.ac.in | 2021101005 |
| Akshat Sanghvi | akshat.sanghvi@students.iiit.ac.in | 2021101094 |
| Keshav Gupta | iamkeshav06@gmail.com | 2021101018 |
| Sreeram Reddy Vennam | vennam404@gmail.com | 2021101045 |

**Outline** ---
| Type | Description | Owner | Deadline |
| ---- | ---------- | ---- | -------- |
| T | Remove the home bar from the PWA app | Dev Team | na |
| I | Look into the Astro framework | na | na |
| T | Define the data model and look into integrating with DynamoDB | Dev Team | 15-03-2023 |
| I | Ping on slack to obtain images for the app | na | na |
| T | Start iterating on the design of the home page and make it more mobile friendly | Dev Team | Indefinite |
| I | There will be an API gateway that triggers a lambda which gets data from the database | na | na |
| T | Read up on AWS triggers | Dev Team | na |
| I | Look into indexed search rather than fuzzy search | na | na |