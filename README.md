# DASS-2023


## Introduction 
  This document serves as a dashboard of the 2023 - DASS projects.
 
## Name of the Company
  Virtual Labs Engineering and Architecture Division

## Primary Work Location
  Work From Home (online)

## Email
  engg@vlabs.ac.in; dass-2023@vlabs.ac.in

## Projects Proposed

  - Project-1 - Virtual Labs Smart Collaboration Platform

  - Project-2 - Progressive Web Application for Virtual Labs 

## Project - 1

  ### Description

  Create a user-friendly, rich, and fast website for Virtual Labs
  content creators, educators, and learners using state-of-the-art
  web development frameworks.

  ### Skill Set 

  Web Technologies, Comparative Analysis of Frameworks, and UI Design

  ### Features

  PWA, Chatbot Training, Chatbot Integration, Dynamic Content Collation from multiple sources, Deployment to Web and Android


  ### Task No 1   
   1. Understand requirements and interface.
   
   2. Identify the engineering requirements.  

  ### Outcome 
  
  A document with recommendations on design and open sourced libraries.

  ### Task No 2
  
   3. Design Website & Build POC using 3 Different Frameworks 

   4. Build and Test the Website using the selected framework 

   5. Chatbot Training and Integration and Deployment 

   6. Meet Performance Requirement - Use Performance Tool to measure and improve the performance to meet the following criteria for each page - 

      a. Page size < 5MB
   
      b. Page load time < 1.5 sec over fast 3G network 
   
  ### Outcome
  
  Webpages should run on Firefox, Chrome, Safari and Edge browser. 

  
  ## Existing Pages 

  - **Website : **Virtual Labs - VLEAD Website**

  - **Source Code: **[Link](https://gitlab.com/vlead-projects/vlead-website/-/tree/dev)**

  - **Hosted URL : **[Link](https://vlead.vlabs.ac.in/)**


## Project - 2

  ### Description

  To build a Progressive Web Application for Virtual Labs.

  ### Skill Set 

  Web Technologies (Javascript, HTML 5, and CSS) and Basics of Cloud and Mobile deployment 

  ### Features

  Building the PWA, Deploying the PWA as an Android App, and Deploying PWA on Windows Store

  ### Task No 1   
   1. Understand requirements and interface.
   
   2. Identify the engineering requirements.  

  ### Outcome 
  
  A document with recommendations on design and open sourced libraries.

  ### Task No 2
  
   3. Implement the Service Worker 

   4. Implement the Home Screen

   5. Test and Deploy to Android and Windows Store 

   6. Meet Performance Requirement - Use Performance Tool to measure and improve the performance to meet the following criteria for each page - 

      a. Page size < 5MB
   
      b. Page load time < 1.5 sec over fast 3G network 
   
  ### Outcome
  
  A PWA for Virtual Labs available on Play Store and Windows Store 


## Number of People
  Four students per project. 

## Team Members and Contact Information  

  TA - Karmanjyot Singh - karmanjyot.singh@students.iiit.ac.in



| SNo  |Team ID   |Members in the team   | Email address/Phone Number  | Github Handle/Gitlab Handle  | Project Repo Link| Hosted Link|
|---|---|---|---|---|---|---|
| 1. |  Project1 |  Yatharth Gupta, Shambhavi Jahagirdar, Druhan Shah, Aryan Chandramania | shambhavi.jahagirdar@research.iiit.ac.in, yatharth.gupta@students.iiit.ac.in, druhan.shah@research.iiit.ac.in , aryan.chandramania@research.iiit.ac.in |  |  |  |  
| 2.| Project2| Akshat Sanghvi, Haran Rajeesh, Sreeram Vennam, Keshav Gupta  | akshat.sanghvi@students.iiit.ac.in haran.raajesh@students.iiit.ac.in, vennam404@gmail.com, iamkeshav06@gmail.com  |   |   |


  
## Working Guidelines 

### Development Process 

Step 1 : Choose one exp from the assigned list and send a mail to systems@vlabs.ac.in with your Github handle and associated email id requesting to be added to the repository.  

Step 2 : Clone the repository.

Step 3 : Populate the dev branches of the repository with the source code of the experiments and unit test the experiments locally. Do not delete gh-pages branch. This is required for automatically deploying the experiment along with UI on GitHub pages for testing the complete experiment .Also do not delete .github directory and the LICENSE file.

Step 4 : Merge the fully tested dev branch to testing branch. This will automatically deploy the experiment along with UI on GitHub pages for testing the complete experiment. You will receive an email about the success/failure of the deployment on the email id associated with the github handle provided in Step 1. 

Step 5 : Set up a demo with the Virtual Labs Team and merge the code with **main branch** only after getting approval from them. 


### Realization in terms of Milestones
  Follow [agile](https://en.wikipedia.org/wiki/Agile_software_development) software development and use [scrum](https://www.scrumalliance.org/why-scrum)  methodology to incrementally working software.  Every project has a product backlog.  In each sprint, items from the product backlog are picked to be realized in a milestone.  Each item from a product backlog translates to one or more tasks and each task is tracked as an issue on
  github.  A milestone is a collection of issues.  It is upto the mentor and the team to choose either one or two week sprints.

  Each release is working software and a release determines the achievement of a milestone.  A project is realized as a series of milestones.

  This planning will be part of the master repo of respective project repos.
  
### Communication 
  Every discussion about the project will be through issues.  Mails are not used for discussion.
  Any informal communication will be through Slack - dass-2023 channel.

### Total person hours  ( As committed by the course coordinator) 
   Design and Analysis of Software Systems (DASS, formerly SSAD) course for the 2nd year students to work on. As you know, the project component carries 40% weightage for the course, and it would be great if the students would get an opportunity to work with you and the team as their
   clients.

   Each student expected to devote 10 hours per week for their projects. It is not 10 hours per team, rather 10 hours per student. Just confirmed it with Prof. Ramesh. There are a total of 12 productive weeks that the student must work for the project. (3 in January, 3 in February, 4 in March and 2 in April). This is excluding the mid-semester week, as that wouldn't be a productive one. There would be 4 members per team, so each team devotes
   40 hours per week and 480 hours in total for the project.

   Since these project are meant to simulate a proper industrial project, they would have to sincerely meet with the clients regularly, document everything, keep track of their status on git and most importantly complete the project and get a good client feedback, else they'd get a bad grade! Kindly requesting you to open up some Virtual Labs projects for the students to work on.
   
### Things done at the weekly meeting

  1. Every meeting starts with a presentation of the code documents and a demonstration.

  2. The pull/merge request is reviewed at the meeting.

  3. Task planning for the next week is discussed.  The students take the meeting notes.  The meeting notes has
     the below structure.  From the meeting, action items are thrashed out and are listed.  The mom file is also part of this repository.  Sample mom is placed under [meetings](https://gitlab.com/vlead-projects/dass-2020/tree/master/%20meeting) directory. You may also refer to the [mom](https://gitlab.com/vlead-projects/experiments/2018-ssad/index/blob/master/src/meetings/2018-08-31-mom.org)
     which captures the mom of all the teams. 

   
     ** Agenda

     ** Discussion Points

     ** Action

     |   |   |   |   |   
     |---|---|---|---| 
     | Item  |  Issue | Artifact   |Status    |   
     | Implement filter|Link to| link to file|In progress|issue |in the repo 


### Work logs
  The work logs of each student will need to be pushed to the repository shared by the TA/Course Coordinator. The work-logs will consider for Grading. 

### Team Meetings
  Team meetings will be held every Wednesday from 4:00 PM till 5:00 PM with a slotted time of 30 min per team.
  


