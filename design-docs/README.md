This directory contains links to documents with recommendations on design and open sourced libraries for each of the DASS project. Please fill the link to Google docs against your respective projects. 


| Project Number | Project Name | Document Link|
|---|---|---|
| 1.|Smart Platform   |   |
| 2.| PWA   |   |
| Sample | [VLSI](https://cse14-iiith.vlabs.ac.in/) | 1. [Library recommendations document](https://docs.google.com/document/d/1rZUoRDmJgvmFf8gX7EBmwWJV1vITfQpmmYILgoNQSTs/edit?usp=sharing) <br> 2. [SRS document](https://docs.google.com/document/d/1h4cqzWmQu6XsfBHZNINpDTNazvzyipA-3f1zfM1XjbM/edit?usp=sharing) |
