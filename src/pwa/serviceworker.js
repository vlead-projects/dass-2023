const urls = ["/", "assets/index-7cf8f7f8.js","assets/index-ba3241c9.css", "index.html", "assets/app-44589c33.webmanifest"];

self.addEventListener("install", event => {
    event.waitUntil(
       caches.open("pwa-assets")
       .then(cache => {
          return cache.addAll(urls);
       })
    );
 });

self.addEventListener("activate", event => {
    console.log("Service worker activated");
});

self.addEventListener("fetch", event => {
    event.respondWith(
      caches.match(event.request)
      .then(cachedResponse => {
          return cachedResponse || fetch(event.request);
      }
    )
   )
 });

{/* <script type="module">
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register("./serviceworker.js");
  }
</script> */}